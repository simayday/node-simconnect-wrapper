# Development

## Tasks

### Build and Run
If you want to build and run the app simply execute the Visual Studio Code "Test" task (default: `Ctrl+Shift+T`)

### Build
If you only want to build, run the Visual Studio Code "Build" task (default: `Ctrl+Shift+B`)

## Useful 3rd party documentation

https://docs.flightsimulator.com/html/Programming_Tools/

http://web.archive.org/web/20081207065854/http://lc0277.nerim.net/jsimconnect/doc/flightsim/simconnect/SimConnect.html

https://github.com/EvenAR/node-simconnect/tree/master/samples/typescript