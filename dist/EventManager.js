"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SimCEventManager = void 0;
const Logger_1 = require("./Logger");
class SimCEventManager extends Logger_1.Logger {
    constructor() {
        super();
        this.eventListeners = {};
    }
    on(eventName, callback) {
        if (!this.eventListeners[eventName]) {
            this.eventListeners[eventName] = [];
        }
        this.eventListeners[eventName].push(callback);
    }
    off(eventName, callback) {
        if (!this.eventListeners[eventName]) {
            return;
        }
        let i = 0;
        while (i < this.eventListeners[eventName].length) {
            if (this.eventListeners[eventName][i] === callback) {
                this.eventListeners[eventName].splice(i, 1);
            }
            else {
                i++;
            }
        }
    }
    dispatch(eventName, data) {
        if (this.eventListeners[eventName]) {
            for (let c of this.eventListeners[eventName]) {
                c(eventName, data);
            }
        }
    }
}
exports.SimCEventManager = SimCEventManager;
//# sourceMappingURL=EventManager.js.map