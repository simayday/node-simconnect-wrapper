"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Logger = void 0;
class Logger {
    debug(...args) {
        console.debug.apply(undefined, args);
    }
    log(...args) {
        console.log.apply(undefined, args);
    }
    info(...args) {
        console.info.apply(undefined, args);
    }
    warn(...args) {
        console.warn.apply(undefined, args);
    }
    error(...args) {
        console.error.apply(undefined, args);
    }
}
exports.Logger = Logger;
//# sourceMappingURL=Logger.js.map