"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const node_simconnect_1 = require("node-simconnect");
const EventManager_1 = require("./EventManager");
const Registrar_1 = require("./Registrar");
/**
 * Wrapper Class for SimConnect
 */
class SimC extends EventManager_1.SimCEventManager {
    constructor(name, reconnectAutomatically) {
        super();
        /**
         * readonly state of the connection
         */
        this.isConnected = false;
        this.name = name;
        this.reconnectAutomatically = reconnectAutomatically === true;
        this.RECONNECT_TIMEOUT = 1000 * 5;
        this.SIMCONNECT_EVENT_NAMES = [
            'event',
            'eventEx1',
            'airportList',
            'vorList',
            'ndbList',
            'waypointList',
            'reservedKey',
            'customAction',
            'clientData',
            'eventWeatherMode',
            'assignedObjectID',
            'eventFilename',
            'eventFrame',
            'eventAddRemove',
            'simObjectData',
            'simObjectDataByType',
            'systemState',
            'weatherObservation',
            'cloudState',
            'eventMultiplayerServerStarted',
            'eventMultiplayerClientStarted',
            'eventMultiplayerSessionEnded',
            'eventRaceEnd',
            'eventRaceLap',
            'facilityData',
            'facilityDataEnd',
            'facilityMinimalList'
        ];
        this.eventRegistrar = new Registrar_1.Registrar();
        this.dataRegistrar = new Registrar_1.Registrar();
        this.pendingDataRequests = {};
        this.dataRequestCounter = 1;
        this.connect();
    }
    connect() {
        this.log('connecting ...');
        (0, node_simconnect_1.open)(this.name, node_simconnect_1.Protocol.FSX_SP2).then(({ recvOpen, handle }) => {
            this.handle = handle;
            this.handle.on('close', () => {
                if (this.isConnected && this.reconnectAutomatically) {
                    this.isConnected = false;
                    this.reconnect();
                }
                this.info('connection-lost');
                this.dispatch('connection-lost');
            });
            this.handle.on('exception', recvException => {
                this.error(recvException);
                this.dispatch('exception', recvException);
            });
            for (let n of this.SIMCONNECT_EVENT_NAMES) {
                this.handle.on(n, evt => this.handleThisEvent(n, evt));
            }
            this.isConnected = true;
            this.info('connection-established');
            this.dispatch('connection-established');
        }).catch(error => {
            this.info('connection-failed', error);
            this.dispatch('connection-failed', error);
            if (this.reconnectAutomatically) {
                this.reconnect();
            }
        });
    }
    reconnect() {
        if (this.handle) {
            this.handle.close();
            this.handle = undefined;
        }
        setTimeout(() => {
            this.connect();
        }, this.RECONNECT_TIMEOUT);
    }
    handleThisEvent(eventName, event) {
        switch (eventName) {
            case 'simObjectData': {
                if (this.pendingDataRequests[event.requestID]) {
                    this.pendingDataRequests[event.requestID].resolve(event.data);
                    delete this.pendingDataRequests[event.requestID];
                }
            }
            default: {
                this.warn('unhandled event type', eventName);
            }
        }
    }
    checkConnection() {
        if (!this.isConnected) {
            throw new Error('Sim is not connected');
        }
    }
    /**
     * triggers an event for a sim object (e.g. aircraft)
     * @param data default: 0
     * @param simObjectId default: 1 (player aircraft)
     */
    simTriggerEvent(simEventId, data, simObjectId) {
        this.checkConnection();
        let id = this.eventRegistrar.getId(simEventId);
        if (id === false) {
            id = this.eventRegistrar.add(simEventId);
            this.handle.mapClientEventToSimEvent(id, simEventId);
        }
        this.handle.transmitClientEvent(simObjectId === undefined ? 1 : simObjectId, id, data == undefined ? 0 : data, 1, 0);
    }
    /**
     * reads a variable from a sim object (e.g. aircraft)
     * @param data default: 0
     * @param simObjectId default: 0 (player aircraft)
     */
    simReadVariable(simVariableId, simVariableUnit, simObjectId) {
        //TODO: implement timeout check
        //TODO: implement check if this fails (problem: we must check the handle.on('exception') call and see if we can get some info from that and then understand which pending request failed
        this.checkConnection();
        return new Promise((resolve, reject) => {
            let id = this.dataRegistrar.getId(simVariableId);
            if (id === false) {
                id = this.dataRegistrar.add(simVariableId);
                this.handle.addToDataDefinition(id, simVariableId, simVariableUnit);
            }
            const requestId = this.dataRequestCounter++;
            this.pendingDataRequests[requestId] = { resolve, reject };
            this.handle.requestDataOnSimObject(requestId, id, simObjectId === undefined ? 0 : simObjectId, node_simconnect_1.SimConnectPeriod.ONCE);
        });
    }
}
let simc = new SimC("My App", true);
simc.on('connection-established', () => {
    /*
    setInterval(()=>{
        simc.simTriggerEvent('MASTER_WARNING_ON')
    }, 1000 * 5)

    setInterval(()=>{
        simc.simReadVariable('GEAR CENTER POSITION', 'percent over 100').then((data: RawBuffer) => console.log('Gear center poosition', data.readFloat64()))
    }, 1000 * 2)*/
    setTimeout(() => {
        //simc.simTriggerEvent('TOGGLE_STATIC_PORT_BLOCKAGE')
        simc.simTriggerEvent('TOGGLE_HYDRAULIC_FAILURE');
        //simc.simTriggerEvent('AUTOPILOT_OFF')
        simc.simTriggerEvent('MASTER_WARNING_ON');
        simc.simTriggerEvent('MASTER_CAUTION_ON');
    }, 1000 * 1);
});
//# sourceMappingURL=app.js.map