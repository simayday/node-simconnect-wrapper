
import { Logger } from "./Logger";

class SimCEventManager extends Logger {
    eventListeners: {[key: string]: Array<Function>};

    constructor(){
        super()

        this.eventListeners = {}
    }


    on(eventName: string, callback: Function){
        if(!this.eventListeners[eventName]){
            this.eventListeners[eventName] = []
        }

        this.eventListeners[eventName].push(callback)
    }

    off(eventName: string, callback: Function){
        if(!this.eventListeners[eventName]){
            return
        }

        let i = 0
        while(i < this.eventListeners[eventName].length){
            if(this.eventListeners[eventName][i] === callback){
                this.eventListeners[eventName].splice(i, 1)
            } else {
                i++
            }
        }
    }

    dispatch(eventName: string, data?: any){
        if(this.eventListeners[eventName]){
            for(let c of this.eventListeners[eventName]){
                c(eventName, data)
            }
        }
    }
}

export { SimCEventManager }