class Logger {

    debug(...args){
        console.debug.apply(undefined, args)
    }

    log(...args){
        console.log.apply(undefined, args)
    }

    info(...args){
        console.info.apply(undefined, args)
    }

    warn(...args){
        console.warn.apply(undefined, args)
    }

    error(...args){
        console.error.apply(undefined, args)
    }
}

export { Logger }