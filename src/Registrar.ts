class Registrar {
    ids: string[]

    constructor(){
        this.ids = ['!!! placeholder to force ids to start at 1 !!!'] // TODO: check if this is necessary
    }

    /**
     * returns false if this name has not been added
     */
    getId(name: string){
        for(let i = 0; i < this.ids.length; i++){
            if (this.ids[i] === name){
                return i
            }
        }

        return false
    }

    add(name: string){
        this.ids.push(name)
        return this.ids.length -1
    }
}

export { Registrar }