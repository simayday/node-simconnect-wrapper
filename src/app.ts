
import {
    open,
    Protocol,
    RawBuffer,
    SimConnectConnection,
    SimConnectPeriod,
    SimConnectRecvEvents
} from "node-simconnect";
import { RecvEvent, RecvSimObjectData } from "node-simconnect/dist/recv";

import { SimCEventManager } from "./EventManager";
import { Registrar } from "./Registrar";


/**
 * Wrapper Class for SimConnect
 */
class SimC extends SimCEventManager {
    handle: SimConnectConnection | undefined;
    isConnected: boolean;
    name: string;
    reconnectAutomatically: boolean;
    RECONNECT_TIMEOUT: number;
    SIMCONNECT_EVENT_NAMES: Array<keyof SimConnectRecvEvents>;
    eventRegistrar: Registrar;
    dataRegistrar: Registrar;
    pendingDataRequests: {[key:number]: {resolve: Function, reject: Function}};
    dataRequestCounter: number;

    constructor(name: string, reconnectAutomatically?: boolean) {
        super()

        /**
         * readonly state of the connection
         */
        this.isConnected = false

        this.name = name

        this.reconnectAutomatically = reconnectAutomatically === true

        this.RECONNECT_TIMEOUT = 1000 * 5

        this.SIMCONNECT_EVENT_NAMES = [
            'event',
            'eventEx1',
            'airportList',
            'vorList',
            'ndbList',
            'waypointList',
            'reservedKey',
            'customAction',
            'clientData',
            'eventWeatherMode',
            'assignedObjectID',
            'eventFilename',
            'eventFrame',
            'eventAddRemove',
            'simObjectData',
            'simObjectDataByType',
            'systemState',
            'weatherObservation',
            'cloudState',
            'eventMultiplayerServerStarted',
            'eventMultiplayerClientStarted',
            'eventMultiplayerSessionEnded',
            'eventRaceEnd',
            'eventRaceLap',
            'facilityData',
            'facilityDataEnd',
            'facilityMinimalList'
        ]

        this.eventRegistrar = new Registrar()
        this.dataRegistrar = new Registrar()

        this.pendingDataRequests = {}
        this.dataRequestCounter = 1

        this.connect()

    }

    private connect(){

        this.log('connecting ...')

        open(this.name, Protocol.FSX_SP2).then(({recvOpen, handle})=>{
            this.handle = handle

            this.handle.on('close', ()=>{
                if(this.isConnected && this.reconnectAutomatically){
                    this.isConnected = false

                    this.reconnect()
                }

                this.info('connection-lost')
                this.dispatch('connection-lost')
            })

            this.handle.on('exception', recvException => {
                this.error(recvException)
                this.dispatch('exception', recvException)
            })

            for(let n of this.SIMCONNECT_EVENT_NAMES){
                this.handle.on(n, evt => this.handleThisEvent(n, evt))
            }



            this.isConnected = true

            this.info('connection-established')
            this.dispatch('connection-established')
        }).catch(error=>{
            this.info('connection-failed', error)
            this.dispatch('connection-failed', error)

            if(this.reconnectAutomatically){
                this.reconnect()
            }
        })
    }

    private reconnect(){
        if(this.handle){
            this.handle.close()
            this.handle = undefined
        }

        setTimeout(()=>{
            this.connect()
        }, this.RECONNECT_TIMEOUT)
    }

    private handleThisEvent(eventName: keyof SimConnectRecvEvents, event: RecvSimObjectData){
        switch(eventName){

            case 'simObjectData': {
                if(this.pendingDataRequests[event.requestID]){
                    this.pendingDataRequests[event.requestID].resolve(event.data)
                    delete this.pendingDataRequests[event.requestID]
                }
            }

            default: {
                this.warn('unhandled event type', eventName)
            }
        }
    }

    private checkConnection(){
        if(!this.isConnected){
            throw new Error('Sim is not connected')
        }
    }

    /**
     * triggers an event for a sim object (e.g. aircraft)
     * @param data default: 0
     * @param simObjectId default: 1 (player aircraft)
     */
    public simTriggerEvent(simEventId: SimConnect_EventId, data?: number, simObjectId?: number){
        this.checkConnection()

        let id = this.eventRegistrar.getId(simEventId)

        if(id === false){
            id = this.eventRegistrar.add(simEventId)

            this.handle.mapClientEventToSimEvent(id, simEventId)
        }

        this.handle.transmitClientEvent(simObjectId === undefined ? 1 : simObjectId, id, data == undefined ? 0 : data, 1, 0)
    }

    /**
     * reads a variable from a sim object (e.g. aircraft)
     * @param data default: 0
     * @param simObjectId default: 0 (player aircraft)
     */
    public simReadVariable(simVariableId: SimConnect_VariableId, simVariableUnit: SimConnect_VariableUnit, simObjectId?: number){
        //TODO: implement timeout check
        //TODO: implement check if this fails (problem: we must check the handle.on('exception') call and see if we can get some info from that and then understand which pending request failed
        this.checkConnection()

        return new Promise((resolve, reject)=>{

            let id = this.dataRegistrar.getId(simVariableId)

            if(id === false){
                id = this.dataRegistrar.add(simVariableId)

                this.handle.addToDataDefinition(id, simVariableId, simVariableUnit)
            }

            const requestId = this.dataRequestCounter++

            this.pendingDataRequests[requestId] = {resolve, reject}

            this.handle.requestDataOnSimObject(requestId, id, simObjectId === undefined ? 0 : simObjectId, SimConnectPeriod.ONCE)

        })
    }
}

type SimConnect_EventId = string // see https://docs.flightsimulator.com/html/Programming_Tools/Event_IDs/Event_IDs.htm
type SimConnect_VariableId = string // see https://docs.flightsimulator.com/html/Programming_Tools/SimVars/Simulation_Variables.htm
type SimConnect_VariableUnit = string // see https://docs.flightsimulator.com/html/Programming_Tools/SimVars/Simulation_Variable_Units.htm




let simc = new SimC("My App", true)

simc.on('connection-established', ()=>{
    /*
    setInterval(()=>{
        simc.simTriggerEvent('MASTER_WARNING_ON')
    }, 1000 * 5)

    setInterval(()=>{
        simc.simReadVariable('GEAR CENTER POSITION', 'percent over 100').then((data: RawBuffer) => console.log('Gear center poosition', data.readFloat64()))
    }, 1000 * 2)*/

    setTimeout(()=>{
        //simc.simTriggerEvent('TOGGLE_STATIC_PORT_BLOCKAGE')
        simc.simTriggerEvent('TOGGLE_HYDRAULIC_FAILURE')
        //simc.simTriggerEvent('AUTOPILOT_OFF')
        simc.simTriggerEvent('MASTER_WARNING_ON')
        simc.simTriggerEvent('MASTER_CAUTION_ON')

    }, 1000 * 1)
})